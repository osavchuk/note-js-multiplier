const multiplierService = require('../services/Multiplier.service')

const calculateIfIsMultipliedThreeAndFive = (req, res) => {
  try {
    const {number} = req.query;
    if (!number) throw new Error('Invalid input data')

    const parsedNumber = parseInt(number)
    if (!parsedNumber) throw new Error('Input data should be integer')

    const result = multiplierService.threeAndFiveChecker(parsedNumber)

    return res.json({result})
  } catch (err) {
    return res.status(400).json({
      status: 400,
      message: err.message
    })
  }
}

module.exports.calculateIfIsMultipliedThreeAndFive = calculateIfIsMultipliedThreeAndFive