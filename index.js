const express = require('express')
const routes = require('./routes')
const app = express()

let PORT = 3000;
app.use(routes)


app.listen(PORT, () => {
  console.log(`Listening ${PORT} port`)
})
