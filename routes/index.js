const router = require('express').Router()

router.use('/multiplier' , require('./multiplier.router'))

module.exports = router