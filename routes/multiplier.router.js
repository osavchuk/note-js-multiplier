const router = require('express').Router()
const controller = require('../controllers/multiplier.contoller')

router.get('/three-and-five', controller.calculateIfIsMultipliedThreeAndFive)

module.exports = router