class MultiplierService {
  /**
   * Checks if input number is a multiple of 3
   * @param {Number} num
   * @returns {boolean}
   */
  isMultipleOfThree(num) {
    return num % 3 === 0
  }

  /**
   * Checks if input number is a multiple of 5
   * @param {Number} num
   * @returns {boolean}
   */
  isMultipleOfFive(num) {
    return num % 5 === 0
  }

  /**
   * Prepares some result by inputs number
   *
   * @param {Number} inputNumber
   * @returns {String|Number}
   */
  threeAndFiveChecker(inputNumber) {
    console.log(`[INFO] Received ${inputNumber} for checking...`)

    if (this.isMultipleOfThree(inputNumber) && this.isMultipleOfFive(inputNumber)) return 'GN'

    if (this.isMultipleOfThree(inputNumber)) return 'G'

    if (this.isMultipleOfFive(inputNumber)) return 'N'

    return inputNumber
  }

}

module.exports = new MultiplierService()