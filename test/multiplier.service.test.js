const multiplierService = require('../services/Multiplier.service')
const chai = require('chai')

describe('MultiplierService -> isMultipleOfThree', () => {
  it('If 30 is multiple 3', () => {
    chai.assert.isTrue(multiplierService.isMultipleOfThree(30))
  })
  it('If 31 is multiple 3', () => {
    chai.assert.isFalse(multiplierService.isMultipleOfThree(31))
  })
})


describe('MultiplierService -> threeAndFiveChecker', () => {
  it('threeAndFiveChecker with 15', () => {
    chai.expect(multiplierService.threeAndFiveChecker(15)).to.be.equal('GN')
  })
  it('threeAndFiveChecker with 9', () => {
    chai.expect(multiplierService.threeAndFiveChecker(9)).to.be.equal('G')
  })
  it('threeAndFiveChecker with 10', () => {
    chai.expect(multiplierService.threeAndFiveChecker(10)).to.be.equal('N')
  })
  it('threeAndFiveChecker with 11', () => {
    chai.expect(multiplierService.threeAndFiveChecker(11)).to.be.equal(11)
  })
})